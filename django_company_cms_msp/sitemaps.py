from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from django_mdat_msp.django_mdat_msp.models import *
from . import views


class MspStaticViewSitemap(Sitemap):
    def items(self):
        return [
            "msp",
            "msp_configuration_management",
            "msp_it_automation",
            "msp_it_documentation",
            "msp_software_development",
            "msp_system_monitoring",
        ]

    def location(self, item):
        return reverse(item)


class MspTopicSitemap(Sitemap):
    def items(self):
        return MdatMspTopics.objects.all().select_related()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(views.msp_topic, args=[obj.id, obj.clean_url])
