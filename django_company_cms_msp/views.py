from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)
from django_mdat_msp.django_mdat_msp.models import MdatMspTopics


def home(request):
    template = loader.get_template("django_company_cms_msp/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("What is that?")

    template_opts["product_msp_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_msp_01_header"
    ).content
    template_opts["product_msp_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_msp_01_content"
    ).content

    template_opts["product_msp_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_msp_02_header"
    ).content
    template_opts["product_msp_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_msp_02_content"
    ).content

    template_opts["product_msp_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_msp_03_header"
    ).content
    template_opts["product_msp_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_msp_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def configuration_management(request):
    template = loader.get_template(
        "django_company_cms_msp/configuration_management.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("Configuration Management")

    template_opts["product_configuration_management_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_01_header"
        ).content
    )

    template_opts["product_configuration_management_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_01_content"
        ).content
    )

    template_opts["product_configuration_management_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_02_header"
        ).content
    )

    template_opts["product_configuration_management_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_02_content"
        ).content
    )

    template_opts["product_configuration_management_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_03_header"
        ).content
    )

    template_opts["product_configuration_management_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_03_content"
        ).content
    )

    template_opts["product_configuration_management_04_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_04_header"
        ).content
    )

    template_opts["product_configuration_management_04_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_04_content"
        ).content
    )

    template_opts["product_configuration_management_msp_topics_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_msp_topics_header"
        ).content
    )

    template_opts["product_configuration_management_msp_topics_descr"] = (
        CompanyCmsGeneral.objects.get(
            name="product_configuration_management_msp_topics_descr"
        ).content
    )

    template_opts["msp_topics"] = MdatMspTopics.objects.order_by("short_name")

    return HttpResponse(template.render(template_opts, request))


def it_automation(request):
    template = loader.get_template("django_company_cms_msp/it_automation.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("IT Automation")

    template_opts["product_it_automation_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_01_header"
    ).content

    template_opts["product_it_automation_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_01_content"
    ).content

    template_opts["product_it_automation_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_02_header"
    ).content

    template_opts["product_it_automation_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_02_content"
    ).content

    template_opts["product_it_automation_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_03_header"
    ).content

    template_opts["product_it_automation_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_it_automation_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def it_documentation(request):
    template = loader.get_template("django_company_cms_msp/it_documentation.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("IT Documentation")

    template_opts["product_it_documentation_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_documentation_01_header"
    ).content

    template_opts["product_it_documentation_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_it_documentation_01_content"
        ).content
    )

    template_opts["product_it_documentation_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_documentation_02_header"
    ).content

    template_opts["product_it_documentation_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_it_documentation_02_content"
        ).content
    )

    template_opts["product_it_documentation_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_documentation_03_header"
    ).content

    template_opts["product_it_documentation_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_it_documentation_03_content"
        ).content
    )

    template_opts["product_it_documentation_04_header"] = CompanyCmsGeneral.objects.get(
        name="product_it_documentation_04_header"
    ).content

    template_opts["product_it_documentation_04_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_it_documentation_04_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def software_development(request):
    template = loader.get_template("django_company_cms_msp/software_development.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("Software Development")

    template_opts["product_software_development_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_01_header"
        ).content
    )

    template_opts["product_software_development_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_01_content"
        ).content
    )

    template_opts["product_software_development_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_02_header"
        ).content
    )

    template_opts["product_software_development_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_02_content"
        ).content
    )

    template_opts["product_software_development_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_03_header"
        ).content
    )

    template_opts["product_software_development_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_software_development_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))


def system_monitoring(request):
    template = loader.get_template("django_company_cms_msp/system_monitoring.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Managed Services Provider")
    template_opts["content_title_sub"] = _("System Monitoring")

    template_opts["product_system_monitoring_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_01_header"
        ).content
    )

    template_opts["product_system_monitoring_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_01_content"
        ).content
    )

    template_opts["product_system_monitoring_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_02_header"
        ).content
    )

    template_opts["product_system_monitoring_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_02_content"
        ).content
    )

    template_opts["product_system_monitoring_03_header"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_03_header"
        ).content
    )

    template_opts["product_system_monitoring_03_content"] = (
        CompanyCmsGeneral.objects.get(
            name="product_system_monitoring_03_content"
        ).content
    )

    return HttpResponse(template.render(template_opts, request))
