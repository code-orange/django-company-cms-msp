from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="msp"),
    path(
        "configuration-management",
        views.configuration_management,
        name="msp_configuration_management",
    ),
    path("it-automation", views.it_automation, name="msp_it_automation"),
    path("it-documentation", views.it_documentation, name="msp_it_documentation"),
    path(
        "software-development",
        views.software_development,
        name="msp_software_development",
    ),
    path("system-monitoring", views.system_monitoring, name="msp_system_monitoring"),
]
